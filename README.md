#Bệnh trĩ ngoại là gì?
BỆNH TRĨ NGOẠI LÀ GÌ?
[bệnh trĩ ngoại là gì](https://phongkhamdaidong.vn/benh-tri-ngoai-la-gi-nguyen-nhan-trieu-chung-va-cach-chua-878.html)? Trĩ ngoại nằm ở phía dưới đường lược, là do đám rối tĩnh mạch bị phình to hoặc những nếp gấp ở vùng da chỗ hậu môn bị viêm, phình đại, mô liên kết tăng lên nhiều hoặc máu bị nghẽn hình thành cục u.
Đặc trưng của bệnh trĩ ngoại là gây tắc động mạch làm cho người bệnh có cảm giác đau đớn.


– Tuy muốn đi ngoài nhưng cảm giác đau đớn khiến họ không thể đại tiện được. Cơ vòng hậu môn đóng, gây ra sự co giật cơ, vì thế người bệnh không thể ngồi, rất khó đi lại, nhiều người có thể phải chịu đau trong vài ngày. Trĩ ngoại tắc động mạch tuy đau đớn nhưng lại thuộc vào loại bệnh dễ điều trị.


– Dù không để ý nhưng sau 5-6 ngày cảm giác đau nhức sẽ giảm đi, một thời gian ngắn sau đó, người bệnh sẽ hết đau. Phương pháp ngồi tắm có thể làm cho máu lưu thông nhanh, bệnh trĩ sẽ tự khỏi. Vì thế trĩ ngoại tắc động mạch thông thường không phải tiến hành phẫu thuật, mà chỉ cần uống thuốc điều trị và tắm rửa, bôi thuốc ngoài là có thể khỏi bệnh. Nhưng khi búi trĩ ngoại to hơn hạt đậu tằm hoặc gây đau đớn dữ dội, tốt nhất người bệnh nên mổ để lấy cục máu đông ở trong đó ra, như vậy mới có thể giảm đau ngay và cũng làm cho bệnh nhanh khỏi. Khi trĩ đã bị vỡ thì bệnh nhân không phải mổ nữa mà chỉ cần nặn cục máu đông trong đó ra là được. Dù trong trường hợp nào cũng không cần nằm viện. Họ có thể đến các phòng khám trị liệu. Búi trĩ ngoại gây tắc động mạch dù không cần điều trị thì cũng tự khỏi, nhưng búi trĩ không được phẫu thuật hay đã phẫu thuật thì sau này vẫn có thể tái phát.

==> Có thể bạn quan tâm: [http://phongkhamdaidong.vn/chi-phi-cat-tri-bang-laser-het-bao-nhieu-tien-2018-877.html](http://phongkhamdaidong.vn/chi-phi-cat-tri-bang-laser-het-bao-nhieu-tien-2018-877.html)


Điều trị và hỗ trợ bệnh trĩ ngoại.

Thuốc uống: gồm các thuốc có thành phần trợ tĩnh mạch, dẫn xuất từ flavonoid. Cơ chế tác động của các thuốc này là bảo vệ vi tuần hoàn, gia tăng trương lực tĩnh mạch, giảm phù nề nhờ tác động chống tắc mạch, kháng viêm tại chỗ, chống nhiễm trùng.

Thực phẩm chức năng được bào chế từ các thảo dược thiên nhiên như Diếp cá, đương qui, tinh chất nghệ, hoa hòe… với các tác dụng hỗ trợ : Giúp cho hệ tĩnh mạch trĩ bền vững, ngăn chặn việc hình thành thêm các búi trĩ. Giúp chống táo bón, chống viêm. Hỗ trợ cải thiện ngay các triệu chứng như chảy máu, ngứa hậu môn, đau rát. An toàn, dùng được cho cả phụ nữ có thai và cho con bú.

Thuốc tại chỗ: gồm các loại thuốc mỡ (pommade) và đạn dược (suppositoire) bao gồm các tác nhân kháng viêm, vô cảm tại chỗ và dẫn xuất trợ tĩnh mạch.

Các biện pháp phòng bệnh trĩ ngoại là:
Thay đổi lối sống của bạn nếu bạn sống một ít vận động. Tập thể dục sẽ thực hiện mông cơ bắp cùng với điều tổng thể của cơ thể.
Tăng cường lượng chất lỏng hàng ngày của bạn và cố gắng bao gồm ít nhất một ly nước ép trái cây tươi hàng ngày.
Tạo một thói quen ăn uống như sợi nhiều càng tốt.

==========================================================================================

Mọi chi tiết xin liên hệ với [phòng khám đa khoa đại đông](https://phongkhamdaidong.vn/)

Tổng đài tư vấn sức khỏe Bệnh viện đa khoa TPHCM

Số điện thoại: 02838 115688 hoặc 02835 921238

Sẵn sàng tư vấn mọi câu hỏi về sức khỏe